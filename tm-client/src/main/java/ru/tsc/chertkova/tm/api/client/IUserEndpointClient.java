package ru.tsc.chertkova.tm.api.client;

import ru.tsc.chertkova.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {
}
