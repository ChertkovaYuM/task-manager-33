package ru.tsc.chertkova.tm.client;

import ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.chertkova.tm.dto.request.task.TaskListRequest;

public class TaskEndpointClientSoap {

    public static void main(String[] args) {
        System.out.println(ITaskEndpoint.newInstance()
                .listTask(new TaskListRequest()));
    }

}
