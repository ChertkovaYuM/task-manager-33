package ru.tsc.chertkova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.client.IDomainEndpointClient;
import ru.tsc.chertkova.tm.dto.request.data.*;
import ru.tsc.chertkova.tm.dto.request.user.UserLoginRequest;
import ru.tsc.chertkova.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.chertkova.tm.dto.request.user.UserProfileRequest;
import ru.tsc.chertkova.tm.dto.response.data.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    public DomainEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull DataJsonFasterXmlLoadRequest request) {
        return call(request, DataJsonFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull DataJsonFasterXmlSaveRequest request) {
        return call(request, DataJsonFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(@NotNull DataJsonJaxbLoadRequest request) {
        return call(request, DataJsonJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(@NotNull DataJsonJaxbSaveRequest request) {
        return call(request, DataJsonJaxbSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull DataXmlFasterXmlLoadRequest request) {
        return call(request, DataXmlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull DataXmlFasterXmlSaveRequest request) {
        return call(request, DataXmlFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(@NotNull DataXmlJaxbLoadRequest request) {
        return call(request, DataXmlJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(@NotNull DataXmlJaxbSaveRequest request) {
        return call(request, DataXmlJaxbSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(@NotNull DataYamlFasterXmlLoadRequest request) {
        return call(request, DataYamlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull DataYamlFasterXmlSaveRequest request) {
        return call(request, DataYamlFasterXmlSaveResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient client = new AuthEndpointClient();
        client.connect();
        System.out.println(client.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getEmail());

        final DomainEndpointClient domainEndpointClient = new DomainEndpointClient(client);
        System.out.println(domainEndpointClient.saveDataBase64(new DataBase64SaveRequest()));

        System.out.println(client.logout(new UserLogoutRequest()));
        client.disconnect();
    }

}
