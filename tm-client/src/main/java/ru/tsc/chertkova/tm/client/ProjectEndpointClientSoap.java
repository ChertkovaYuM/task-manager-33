package ru.tsc.chertkova.tm.client;

import ru.tsc.chertkova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.chertkova.tm.dto.request.project.ProjectListRequest;

public class ProjectEndpointClientSoap {

    public static void main(String[] args) {
        System.out.println(IProjectEndpoint.newInstance()
                .listProject(new ProjectListRequest()));
    }

}
