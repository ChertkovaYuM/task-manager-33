package ru.tsc.chertkova.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AbstractSoapClient {

    private String host="localhost";

    private String port ="8080";

}
