package ru.tsc.chertkova.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.client.ITaskEndpointClient;
import ru.tsc.chertkova.tm.dto.request.task.*;
import ru.tsc.chertkova.tm.dto.response.task.*;

public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUnbindToProjectResponse unbindTaskToProject(@NotNull TaskUnbindToProjectRequest request) {
        return call(request, TaskUnbindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskStatusById(@NotNull TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskShowListByProjectIdResponse showListByProjectIdTask(@NotNull TaskShowListByProjectIdRequest request) {
        return call(request, TaskShowListByProjectIdResponse.class);
    }

}
