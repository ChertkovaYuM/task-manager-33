package ru.tsc.chertkova.tm.client;

import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.chertkova.tm.dto.request.user.UserLoginRequest;

public class AuthEndpointClientSoap {

    public static void main(String[] args) {
        System.out.println(IAuthEndpoint.newInstance().login(new UserLoginRequest()));
    }

}
