package ru.tsc.chertkova.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.client.IEndpointClient;
import ru.tsc.chertkova.tm.dto.response.ApplicationErrorResponse;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @Nullable
    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @SneakyThrows
    protected <T> T call(final Object data, Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @SneakyThrows
    private ObjectInputStream getObjectInputStream() {
        return new ObjectInputStream(getInputStream());
    }

    @SneakyThrows
    private OutputStream getOutputStream() {
        return socket.getOutputStream();
    }

    @SneakyThrows
    private InputStream getInputStream() {
        return socket.getInputStream();
    }

    @NotNull
    @SneakyThrows
    public Socket connect() {
        socket = new Socket(host, port);
        return socket;
    }

    @NotNull
    @SneakyThrows
    public Socket disconnect() {
        socket.close();
        return socket;
    }

}
