package ru.tsc.chertkova.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.model.ICommand;
import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IServiceLocator;
import ru.tsc.chertkova.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    public abstract void execute();

    @Getter
    @Setter
    @Nullable
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    public void showError(@Nullable final String param) {
        System.out.printf("Error! This argument '%s' not supported...\n", param);
    }

}
