package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.client.*;

public interface IServiceLocator {

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ITaskEndpointClient getTaskEndpoint();

    @NotNull
    IProjectEndpointClient getProjectEndpoint();

    @NotNull
    ISystemEndpointClient getSystemEndpoint();

    @NotNull
    IAuthEndpointClient getAuthEndpoint();

    @NotNull
    IDomainEndpointClient getDomainEndpoint();

    @NotNull
    IUserEndpointClient getUserEndpoint();

}
