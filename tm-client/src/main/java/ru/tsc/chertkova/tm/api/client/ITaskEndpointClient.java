package ru.tsc.chertkova.tm.api.client;

import ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint;

public interface ITaskEndpointClient extends ITaskEndpoint, IEndpointClient {
}
