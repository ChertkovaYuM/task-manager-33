package ru.tsc.chertkova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.client.IAuthEndpointClient;
import ru.tsc.chertkova.tm.dto.request.user.UserLoginRequest;
import ru.tsc.chertkova.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.chertkova.tm.dto.request.user.UserProfileRequest;
import ru.tsc.chertkova.tm.dto.response.user.UserLoginResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserLogoutResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient client = new AuthEndpointClient();
        client.connect();
        System.out.println(client.profile(new UserProfileRequest()).getUser());
        System.out.println(client.login(new UserLoginRequest("test2", "test2")).getSuccess());
        System.out.println(client.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(client.logout(new UserLogoutRequest()));
        System.out.println(client.profile(new UserProfileRequest()).getUser());
        client.disconnect();
    }

}
