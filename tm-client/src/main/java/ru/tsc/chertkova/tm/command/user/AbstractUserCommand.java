package ru.tsc.chertkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.client.IUserEndpointClient;
import ru.tsc.chertkova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    public IUserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

}
