package ru.tsc.chertkova.tm.client;

import ru.tsc.chertkova.tm.api.endpoint.IUserEndpoint;
import ru.tsc.chertkova.tm.dto.request.user.UserLockRequest;

public class UserEndpointClientSoap {

    public static void main(String[] args) {
        System.out.println(IUserEndpoint.newInstance()
                .lockUser(new UserLockRequest()));
    }

}
