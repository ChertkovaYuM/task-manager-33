package ru.tsc.chertkova.tm.client;

import lombok.SneakyThrows;
import ru.tsc.chertkova.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.chertkova.tm.dto.request.system.ApplicationAboutRequest;

public class SystemEndpointClientSoap {

    @SneakyThrows
    public static void main(String[] args) {
//        final String wsdl = "http://localhost:8080/SystemEndpoint?wsdl";
//        final ISystemEndpoint endpoint = Service.create(
//                new URL(wsdl),
//                new QName("http://endpoint.tm.chertkova.tsc.ru/", "SystemEndpointService")
//        ).getPort(ISystemEndpoint.class);
        System.out.println(ISystemEndpoint.newInstance()
                .getAbout(new ApplicationAboutRequest()).getEmail());
    }

}
