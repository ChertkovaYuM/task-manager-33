package ru.tsc.chertkova.tm.client;

import ru.tsc.chertkova.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.chertkova.tm.dto.request.data.DataBackupSaveRequest;

public class DomainEndpointClientSoap {

    public static void main(String[] args) {
        System.out.println(IDomainEndpoint.newInstance()
                .saveDataBackup(new DataBackupSaveRequest()));
    }

}
