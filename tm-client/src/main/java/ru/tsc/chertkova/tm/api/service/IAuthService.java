package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.component.Bootstrap;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User getUser();

    @Nullable
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    User check(@Nullable String login, @Nullable String password);

}
