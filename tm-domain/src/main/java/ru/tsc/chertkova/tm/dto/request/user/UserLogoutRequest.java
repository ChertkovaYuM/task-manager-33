package ru.tsc.chertkova.tm.dto.request.user;

import lombok.NoArgsConstructor;
import ru.tsc.chertkova.tm.dto.request.AbstractRequest;

@NoArgsConstructor
public final class UserLogoutRequest extends AbstractRequest {
}
