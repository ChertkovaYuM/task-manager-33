package ru.tsc.chertkova.tm.dto.request.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.AbstractRequest;

@Getter
@Setter
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String userId;

}
