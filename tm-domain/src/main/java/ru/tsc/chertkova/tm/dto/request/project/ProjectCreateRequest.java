package ru.tsc.chertkova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public ProjectCreateRequest(@Nullable String name, @Nullable String description) {
        this.name = name;
        this.description = description;
        this.dateBegin = new Date();
        this.dateEnd = new Date("dd.mm.yyyy+1");
    }

}
