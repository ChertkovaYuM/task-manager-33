package ru.tsc.chertkova.tm.dto.request.data;

import lombok.NoArgsConstructor;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

@NoArgsConstructor
public final class DataBackupSaveRequest extends AbstractUserRequest {
}
