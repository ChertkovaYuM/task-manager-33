package ru.tsc.chertkova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.user.AccessDeniedException;
import ru.tsc.chertkova.tm.model.User;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    protected AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = getServiceLocator().getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        @NotNull final Role roleUser = user.getRole();
        if (roleUser != role) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

}
