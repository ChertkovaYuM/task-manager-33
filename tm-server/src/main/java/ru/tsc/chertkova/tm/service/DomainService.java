package ru.tsc.chertkova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.IDomainService;
import ru.tsc.chertkova.tm.component.Bootstrap;

public class DomainService implements IDomainService {

    @Getter
    @Nullable
    private final Bootstrap bootstrap;

    public DomainService(@Nullable Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void loadDataBackup() {
    }

    @Override
    public void saveDataBackup() {
    }

    @Override
    public void loadDataBase64() {
    }

    @Override
    public void saveDataBase64() {
    }

    @Override
    public void loadDataBinary() {
    }

    @Override
    public void saveDataBinary() {
    }

    @Override
    public void loadDataJsonFasterXml() {
    }

    @Override
    public void saveDataJsonFasterXml() {
    }

    @Override
    public void loadDataJsonJaxb() {
    }

    @Override
    public void saveDataJsonJaxb() {
    }

    @Override
    public void loadDataXmlFasterXml() {
    }

    @Override
    public void saveDataXmlFasterXml() {
    }

    @Override
    public void loadDataXmlJaxb() {
    }

    @Override
    public void saveDataXmlJaxb() {
    }

    @Override
    public void loadDataYamlFasterXml() {
    }

    @Override
    public void saveDataYamlFasterXml() {
    }

}
