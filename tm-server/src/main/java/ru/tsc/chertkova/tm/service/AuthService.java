package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.exception.user.AccessDeniedException;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @Nullable
    private final IPropertyService propertyService;

    @Nullable
    private final IUserService userService;

    public AuthService(@Nullable final IPropertyService propertyService,
                       @Nullable final IUserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    @Nullable
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new UserNotFoundException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

}
